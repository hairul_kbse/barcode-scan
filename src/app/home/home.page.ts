import { Component, OnInit } from '@angular/core';
import { Todo, Item, Order, TodoService } from '../services/todo.service';
import { NavController, LoadingController } from '@ionic/angular';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
 
  scannedData: {};
  order: Order;
  orders: Order[];
  todo: Todo;
  todos: Todo[];
  item: Item;
  total_sum = 0;
  barcodeScannerOptions: BarcodeScannerOptions;
 
  constructor(private nav: NavController, private loadingController: LoadingController, private todoService: TodoService, private barcodeScanner: BarcodeScanner) { 



  }
 
  ngOnInit() {

    this.todoService.getOrders().subscribe(res => {

      this.total_sum = 0;

      this.orders = res;
      res.forEach((re) => {

        this.total_sum += re.price;

      })
    });

    //Options
    this.barcodeScannerOptions = {
      showTorchButton: true,
      showFlipCameraButton: true
    };
  }



  scanCode() {

    this.barcodeScanner.scan().then(barcodeData => {
      

      this.todoService.getItem(barcodeData.text).subscribe(res => {

        this.item = res;

        this.order = {
          itemName: this.item.name,
          quantity: 1,
          total: this.item.price*1,
          price: this.item.price
        };

        this.todoService.addOrder(this.order);

      });
    }).catch(err => {
      console.log('Error', err);

      alert(JSON.stringify(err));
    });

  }
 
  remove(item) {
    this.todoService.removeOrder(item.id);
  }

  clearList(){

    this.todoService.removeAllOrder();

    
  }
}
