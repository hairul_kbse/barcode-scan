import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Todo {
  id?: string;
  task: string;
  priority: number;
  createdAt: number;
}

export interface Item {
  id?: string;
  name: string;
  price: number;
  quantity: number;
}

export interface Order {
  id?: string,
  itemName: string,
  quantity: number,
  total: number,
  price: number
}

@Injectable({
  providedIn: 'root'
})
export class TodoService {



  private ordersCollection: AngularFirestoreCollection<Order>;
  private removeOrdersCollection: AngularFirestoreCollection<Order>;

  private todosCollection: AngularFirestoreCollection<Todo>;
 
  private todos: Observable<Todo[]>;

  private orders: Observable<Order[]>;

  private orderList: Order[];

  private removeOrders: Observable<Order[]>;

  private itemsCollection: AngularFirestoreCollection<Item>;
 
  constructor(db: AngularFirestore) {

    this.todosCollection = db.collection<Todo>('todos');
 
    this.todos = this.todosCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );

    this.itemsCollection = db.collection<Item>('items');

    this.ordersCollection = db.collection<Order>('orders');

    this.removeOrdersCollection = db.collection<Order>('orders');

    this.orders = this.ordersCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );

    this.removeOrders = this.removeOrdersCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );

  }
 
  getTodos() {
    return this.todos;
  }

  getOrders(){
    return this.orders;
  }

  getRemoveOrders(){
    return this.removeOrders;
  }
 
  getTodo(id) {
    return this.todosCollection.doc<Todo>(id).valueChanges();
  }

  getItem(id){
    return this.itemsCollection.doc<Item>(id).valueChanges();
  }
 
  updateTodo(todo: Todo, id: string) {
    return this.todosCollection.doc(id).update(todo);
  }

  updateOrder(order: Order, id: string) {
    return this.ordersCollection.doc(id).update(order);
  }
 
  addTodo(todo: Todo) {
    return this.todosCollection.add(todo);
  }

  addOrder(order: Order)
  {
    return this.ordersCollection.add(order);
  }
 
  removeTodo(id) {
    return this.todosCollection.doc(id).delete();
  }

  removeOrder(id){
    return this.removeOrdersCollection.doc(id).delete();
  }

  removeAllOrder()
  {
    var a = this.getRemoveOrders().subscribe(frs => {

      this.orderList = frs;

      // frs.forEach(fr => {
      //   this.removeOrder(fr.id);
      // });
    });

    //a.unsubscribe();

    this.orderList.forEach(ord => {
      this.removeOrder(ord.id);
    });
  }
}
